import { createStackNavigator, createAppContainer } from 'react-navigation'
import TodoListScreen from '../Containers/TodoListScreen'
// import DetailPegawaiScreen from '../Containers/DetailPegawaiScreen'
// import PegawaiScreen from '../Containers/PegawaiScreen'
// import CatatanScreen from '../Containers/CatatanScreen'
import LaunchScreen from '../Containers/LaunchScreen'

import styles from './Styles/NavigationStyles'

// Manifest of possible screens
const PrimaryNav = createStackNavigator({
  TodoListScreen: { screen: TodoListScreen },
  // DetailPegawaiScreen: { screen: DetailPegawaiScreen },
  // PegawaiScreen: { screen: PegawaiScreen },
  // CatatanScreen: { screen: CatatanScreen },
  LaunchScreen: { screen: LaunchScreen }
}, {
  // Default config for all screens
  headerMode: 'none',
  initialRouteName: 'TodoListScreen',
  navigationOptions: {
    headerStyle: styles.header
  }
})

export default createAppContainer(PrimaryNav)
