import { createReducer, createActions } from 'reduxsauce'
import Immutable from 'seamless-immutable' 

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
  pegawaiRequest: ['data'],
  pegawaiSuccess: ['payload'],
  pegawaiFailure: null,

  pegawaiGantiNilaiSaya: null,

})

export const PegawaiTypes = Types
export default Creators

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
  data: null,
  fetching: null,
  payload: [],
  error: null,

  nilaisaya: 'Tekan'
})

/* ------------- Selectors ------------- */

export const PegawaiSelectors = {
  getData: state => state.pegawai.data
}

/* ------------- Reducers ------------- */

// request the data from an api 
export const request = (state, { data }) =>
  state.merge({ fetching: true, data, payload: [] })

// successful api lookup
export const success = (state, { payload }) => {
  // const { payload } = action
  return state.merge({ fetching: false, error: null, payload })
}

// Something went wrong somewhere.
export const failure = state =>
  state.merge({ fetching: false, error: true, payload: null })

export const gantinilaisaya = state =>
  state.merge({ nilaisaya: (Math.random() * 10).toString() }) 

/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.PEGAWAI_REQUEST]: request,
  [Types.PEGAWAI_SUCCESS]: success,
  [Types.PEGAWAI_FAILURE]: failure,

  [Types.PEGAWAI_GANTI_NILAI_SAYA]: gantinilaisaya
})
