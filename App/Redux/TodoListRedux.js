import { createReducer, createActions } from 'reduxsauce'
import Immutable from 'seamless-immutable'

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
  todoListRequest: ['data'],
  todoListSuccess: ['payload'],
  todoListFailure: null,

  todoListGenerateRandom: null
})

export const TodoListTypes = Types
export default Creators

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
  data: null,
  fetching: null,
  payload: null,
  error: null,

  randomnumber: '0',
})

/* ------------- Selectors ------------- */

export const TodoListSelectors = {
  getData: state => state.data
}

/* ------------- Reducers ------------- */

// request the data from an api
export const request = (state, { data }) =>
  state.merge({ fetching: true, data, payload: null })

// successful api lookup
export const success = (state, action) => {
  const { payload } = action
  return state.merge({ fetching: false, error: null, payload })
}

// Something went wrong somewhere.
export const failure = state =>
  state.merge({ fetching: false, error: true, payload: null })


export const generatenumber = state =>
  state.merge({ randomnumber: (Math.random() * 10).toString() })
/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.TODO_LIST_REQUEST]: request,
  [Types.TODO_LIST_SUCCESS]: success,
  [Types.TODO_LIST_FAILURE]: failure,

  [Types.TODO_LIST_GENERATE_RANDOM]: generatenumber,
})
