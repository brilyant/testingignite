import React, { Component } from 'react'
import { ScrollView, Button, KeyboardAvoidingView, Text } from 'react-native'
import { connect } from 'react-redux'
// Add Actions - replace 'Your' with whatever your reducer is called :)
import PegawaiActions from '../Redux/PegawaiRedux'

// Styles
import styles from './Styles/PegawaiScreenStyle'

class PegawaiScreen extends Component {
  render () {
    return (
      <ScrollView style={[styles.container]}>
        <KeyboardAvoidingView behavior='position'>
          <Button
            title="Kita Pindah"
            onPress={() => {
              console.log('Pindah ke detail')              
              this.props.navigation.navigate('DetailPegawaiScreen') 
            }}
          />

          <Button
            color="red"
            title={this.props.pegawai.nilaisaya}
            onPress={() => { 
              this.props.pegawaiGantiNilaiSaya()
            }}
          /> 
        </KeyboardAvoidingView>
      </ScrollView>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    pegawai: state.pegawai,
    search: state.search,
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    pegawaiGantiNilaiSaya: () => dispatch(PegawaiActions.pegawaiGantiNilaiSaya()),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(PegawaiScreen)
