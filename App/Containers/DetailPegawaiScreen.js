import React, { Component } from 'react'
import { ScrollView, Text, KeyboardAvoidingView, Button, ActivityIndicator } from 'react-native'
import { connect } from 'react-redux'
// Add Actions - replace 'Your' with whatever your reducer is called :)
import PegawaiActions from '../Redux/PegawaiRedux'

// Styles
import styles from './Styles/DetailPegawaiScreenStyle'

class DetailPegawaiScreen extends Component {
  render () {
    if (this.props.pegawai.fetching === true) {
      return (
        <ActivityIndicator />
      )
    }
    if (this.props.pegawai.error === true) {
      return(<Text>Error</Text>)
    }
    return (
      <ScrollView style={styles.container}>
        <KeyboardAvoidingView behavior='position'>
          <Button
            title="Get Data"
            onPress={() => { 
              this.props.pegawaiRequest('5')
            }}
          />
          <Text>{JSON.stringify(this.props.pegawai.payload)}</Text>
        </KeyboardAvoidingView>
      </ScrollView>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    pegawai: state.pegawai
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    pegawaiRequest: (data) => dispatch(PegawaiActions.pegawaiRequest(data)),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(DetailPegawaiScreen)
