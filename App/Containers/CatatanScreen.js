import React, { Component } from 'react'
import { ScrollView, Text, KeyboardAvoidingView, TouchableHighlight, ActivityIndicator } from 'react-native'
import { connect } from 'react-redux'
// Add Actions - replace 'Your' with whatever your reducer is called :)
// import YourActions from '../Redux/YourRedux'
import CatatanActions from '../Redux/CatatanRedux'

// Styles
import styles from './Styles/CatatanScreenStyle'

class CatatanScreen extends Component {
  componentDidMount() {
    this.props.catatanRequest()
  }

  render () {
    return (
      <ScrollView style={styles.container}>
        <KeyboardAvoidingView behavior='position'>
          <TouchableHighlight onPress={() => this.props.iniCustomLoh((Math.random() * 10))}>
            {
              (this.props.catatan.fetching === true) ? <ActivityIndicator color="green" /> :
                (this.props.catatan.error === true) ? <Text>gagal terhubung</Text> :
                  <Text>{JSON.stringify(this.props.catatan.payload)}</Text>
            }
          </TouchableHighlight>
        </KeyboardAvoidingView>
      </ScrollView>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    catatan: state.catatan
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    iniCustomLoh: (param) => dispatch(CatatanActions.iniCustomLoh(param)),
    catatanRequest: (data) => dispatch(CatatanActions.catatanRequest(data)),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(CatatanScreen)
