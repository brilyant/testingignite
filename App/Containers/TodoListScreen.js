import React, { Component } from 'react'
import { ScrollView, Text, KeyboardAvoidingView, Button, ActivityIndicator } from 'react-native'
import { connect } from 'react-redux'
// Add Actions - replace 'Your' with whatever your reducer is called :)
import TodoListActions from '../Redux/TodoListRedux'

// Styles
import styles from './Styles/TodoListScreenStyle'

class TodoListScreen extends Component {
  render () {
    return (
      <ScrollView style={styles.container}>
        <KeyboardAvoidingView behavior='position'>
          <Text>TodoListScreen</Text>
          <Button
            title={'Ambil Data'}
            onPress={() => {
              console.log('action')
              //taruh action disini
              this.props.todoListRequest('2')
            }}
          />
          {
            (this.props.todolist.fetching === true) ? 
              <ActivityIndicator color="red" /> :
              <Text>{JSON.stringify(this.props.todolist.payload)}</Text>
          }
        </KeyboardAvoidingView>
      </ScrollView>
    )
  }
}


const mapStateToProps = (state) => {
  return {
    todolist: state.todolist
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    todoListGenerateRandom: () => dispatch(TodoListActions.todoListGenerateRandom()),
    todoListRequest: (data) => dispatch(TodoListActions.todoListRequest(data)),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(TodoListScreen)
